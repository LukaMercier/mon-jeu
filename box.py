class box(object):
    def __init__(self,capacite,status=True):
        self._contents=[]
        self._status=status
        self._capacite=capacite

    def __contains__(self,machin):
        return machin in self._contents

    def add(self,truc,taille):
        if self._status and truc.getvolume()<=self._capacite:
            self._contents.append(truc.getnom())
            self._capacite-=taille

    def delete(self,thing):
        if self._status:
            self._contents.remove(thing.getnom())
            self._capacite+=thing.getvolume()
    def is_open(self):
        return self._status

    def open(self):
        self._status=True

    def close(self):
        self._status=False

    def action_look(self):
        if self.is_open():
            if len(self._contents)>0:
                res="La boite contient: "
                for elem in self._contents:
                    res+=elem+", "
                return res
            else:
                return "La boite est vide"
        else:
            return "La boite est fermée"


class Thing(object):
    def __init__(self,nom,volume):
        self._nom=nom
        self._volume=volume

    def getnom(self):
        return self._nom

    def getvolume(self):
        return self._volume
